# Class: squid::package

class squid::package  inherits squid {

  package{$::squid::package_name:
    ensure => latest,
  }

}
