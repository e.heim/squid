# == Class: installs an configures squid proxy on redhat based os 
class squid::config (
) inherits squid {

  file { '/etc/squid/squid.conf':
        notify  => Service['squid'],
        mode    => '0640',
        owner   => 'root',
        group   => 'squid',
        content => template('squid/squid.conf.erb'),
    }
}
