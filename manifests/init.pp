# == Class: installs an configures squid proxy on redhat based os 
class squid (
  $squid_ip ='',
  $squid_port = '3128',
  $ensure_service                = 'running',
  $enable_service                = true,
  $cache_mem                     = '256 MB',
  $maximum_object_size_in_memory = '32 MB',
  $package_name                  = 'squid',
  $service_name                  = 'squid',
  )
  {

  anchor{'squid::begin':}
  -> class{'::squid::package':}
  ~> class{'::squid::config':}
  ~> class{'::squid::service':}
  -> anchor{'squid::end-':}

  }
